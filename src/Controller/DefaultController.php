<?php

namespace App\Controller;
use App\Entity\Users;
use function Sodium\add;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/users")
 */
class DefaultController extends Controller {
    /**
     * @Route("/")
     */
    public function index() {
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();
        return $this->render('users/accueil.html.twig',['users'=>$users]);
    }

    /**
     * @Route("/create")
     */
    public function create(Request $request) {
        $db = $this->getDoctrine()->getManager();
        $user = new Users();
        $form = $this->createFormBuilder($user)
            ->add('Nom',TextType::class)
            ->add('Prenom',TextType::class)
            ->add('Age',IntegerType::class)
            ->add('Submit',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        $data = $form->getData();
        if($form->isSubmitted() && $form->isValid()) {
            $db->persist($data);
            $db->flush();
            return $this->redirect('/users');
        }
        return $this->render('users/index.html.twig',['form'=>$form->createView()]);
    }
    /**
     * @Route("/update/{id}")
     */
    public function update(Request $request, $id){
        $db = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(Users::class)->find($id);
        $form = $this->createFormBuilder($user)
            ->add('Nom',TextType::class)
            ->add('Prenom',TextType::class)
            ->add('Age',IntegerType::class)
            ->add('Submit',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $db->flush();
            return $this->redirect('/users');
        }
        return $this->render('users/index.html.twig',['form'=>$form->createView()]);
    }
    /**
     * @Route("/delete/{id}")
     */
    public function delete($id){
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository(Users::class)->find($id);
        $entityManager->remove($users);
        $entityManager->flush();
        return $this->redirect('/users');
    }
}